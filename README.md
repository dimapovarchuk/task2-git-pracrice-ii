# GIT Basics homework II

## Part 1 (do 'GIT Basics homework' first)

### Checkout on ”develop” in directory  “task2 – git pracrice II”.
<img width="682" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/745366a3-bdb1-4d9e-a9c7-e2b3221c5a21">

### Created index.html in new folder “task2 – git pracrice II” and commited it.
<img width="704" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/c0387540-edd8-4d22-90de-4525b416f4dc">

### Created new branch “first”.
<img width="677" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/ab0a205e-a84e-4a2f-bd4c-05f2be97ac33">

### Created another new branch “second”.
<img width="680" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/b318c147-321c-4a56-b657-cf7fde359564">

### Did commits to “develop” (Added to repository some files, changed them).
<img width="740" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/c39e1869-dcb7-487d-80ea-d0a6d87fec6e">
<img width="934" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/aa4d9c04-169b-4617-92cb-bf7e4caf56f3">

### Checkouted on the “first” branch and did several commits.
<img width="712" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/eec4aeb8-ddc4-449b-8a48-c975abcb3e95">

### Checkouted on the “second” branch and did several commits.
<img width="710" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/eb84dffe-d3b4-484a-994b-a302841bd39b">

### Did rebasing from "second" to "develop" branch.
<img width="670" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/91bb32c3-d774-4d58-85ef-876347d9925d">
<img width="710" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/97992c6a-8861-4daf-bbd1-c7835338c1fc">

### Merged “second” branch into develop (fast-forward).
<img width="715" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/99aa3d92-6608-4277-bc7b-1d7edcc4880f">

### Checkouted to the ”first” branch.
<img width="669" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/7355b797-29d2-42ec-a113-7e938d3c3c2c">

### Merged “first” branch into develop.
<img width="700" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/73367014-edf7-4f85-ab35-664cafe18bd8">
<img width="658" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/d6138020-1d0c-47ea-813e-69febed1d302">

### Merged the “develop” branch into “master” branch
<img width="661" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/379372ae-d14e-4e87-9c18-2347c07109f6">

### Pushed all your local branches to origin.
<img width="945" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/3cde7576-754d-42e3-8bd0-aa63f26e4966">

### Git reflog.
<img width="944" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/02b1bef7-d65b-408f-a2f2-1434295c3dfc">
<img width="955" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/974120db-49da-42ce-8f58-abc97f38f5bc">
